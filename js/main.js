var config = {
  type: Phaser.Auto,
  width: 1000,
  height: 720,
  scene: {
    preload: preload,
    create: create,
    update: update
  }
};

game = new Phaser.Game(config);

function preload() {
  this.load.image('background', 'img/background.jpg');
  this.load.image('pea-plant', 'img/pea-plant.png');
  this.load.image('peas', 'img/peas.png');
  this.load.image('sun', 'img/sun.png');
  this.load.image('sunflower', 'img/sunflower.png');
  this.load.image('zombie', 'img/zombie.png');
}

function create() {
  this.add.image(500, 340, 'background');
  this.add.image(100, 340, 'pea-plant');
  this.add.image(200, 340, 'peas');
  this.add.image(300, 340, 'sun');
  this.add.image(400, 340, 'sunflower');
  this.add.image(500, 340, 'zombie');


}

function update() {

  
}
